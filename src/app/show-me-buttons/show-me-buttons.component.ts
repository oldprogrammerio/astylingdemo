import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-show-me-buttons',
  templateUrl: './show-me-buttons.component.html',
  styleUrls: ['./show-me-buttons.component.scss']
})
export class ShowMeButtonsComponent implements OnInit {
  public segmentNumber = 3;
  public countOfSegments = 5;
  constructor() { }

  ngOnInit(): void {
  }

}
