import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowMeButtonsComponent } from './show-me-buttons.component';

describe('ShowMeButtonsComponent', () => {
  let component: ShowMeButtonsComponent;
  let fixture: ComponentFixture<ShowMeButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowMeButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowMeButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
